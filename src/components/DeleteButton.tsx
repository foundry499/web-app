import React from 'react';
import {
    Button,
    ButtonGroup,
    IconButton,
    Popover,
    PopoverTrigger,
    PopoverContent,
    PopoverArrow,
    PopoverCloseButton,
    PopoverBody,
    PopoverHeader,
    PopoverFooter,
} from '@chakra-ui/core';

type Props = {
    handleDelete(): void;
    msg: string;
};

export default ({ handleDelete, msg }: Props) => {
    const initRef = React.useRef(null);
    return (
        <Popover initialFocusRef={initRef}>
            {({ onClose }) => (
                <>
                    <PopoverTrigger>
                        <IconButton
                            variant="outline"
                            variantColor="red"
                            aria-label="cancel job"
                            fontSize="20px"
                            icon="delete"
                        />
                    </PopoverTrigger>
                    <PopoverContent zIndex={4}>
                        <PopoverArrow />
                        <PopoverCloseButton />
                        <PopoverHeader fontWeight="semibold">Confirm</PopoverHeader>
                        <PopoverBody>{msg}</PopoverBody>
                        <PopoverFooter>
                            <ButtonGroup size="sm">
                                <Button variant="outline" onClick={onClose} ref={initRef}>
                                    Cancel
                                </Button>
                                <Button onClick={handleDelete} variantColor="red" aria-label="cancel job">
                                    Delete
                                </Button>
                            </ButtonGroup>
                        </PopoverFooter>
                    </PopoverContent>
                </>
            )}
        </Popover>
    );
};
