import React, { FunctionComponent } from 'react';
import { AuthContext } from '../App';
import {
    Box,
    Heading,
    Flex,
    Text,
    Button,
    MenuList,
    MenuDivider,
    MenuItem,
    Menu,
    MenuButton,
    Link,
    FlexProps,
} from '@chakra-ui/core';
import { Link as ReachLink } from '@reach/router';

const MenuItems: FunctionComponent<{}> = ({ children }) => (
    <Text mt={{ base: 4, md: 0 }} mr={6} display="block">
        {children}
    </Text>
);

const Header = (props: FlexProps) => {
    const [show, setShow] = React.useState(false);
    const { state, dispatch } = React.useContext(AuthContext);
    const role = state.user?.role;

    const handleToggle = () => setShow(!show);

    const onLogout = () => {
        dispatch({
            type: 'LOGOUT',
        });
    };

    return (
        <>
            <Flex
                as="nav"
                align="center"
                justify="space-between"
                wrap="wrap"
                padding="1.5rem"
                bg="brand.900"
                color="white"
                {...props}
            >
                <Flex align="center" mr={5}>
                    <Heading as="h1" size="lg" letterSpacing={'-.1rem'}>
                        {/*
                    // @ts-ignore * wait for chakra update */}
                        <Link as={ReachLink} to="/">
                            Foundry499
                        </Link>
                    </Heading>
                </Flex>

                <Box display={{ sm: 'block', md: 'none' }} onClick={handleToggle}>
                    <svg fill="white" width="12px" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <title>Menu</title>
                        <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z" />
                    </svg>
                </Box>

                <Box
                    display={{ sm: show ? 'block' : 'none', md: 'flex' }}
                    width={{ sm: 'full', md: 'auto' }}
                    alignItems="center"
                    flexGrow={1}
                >
                    <MenuItems>
                        {/*
                    // @ts-ignore * wait for chakra update */}
                        <Link as={ReachLink} to="/job">
                            Jobs
                        </Link>
                    </MenuItems>

                    <MenuItems>
                        {/*
                    // @ts-ignore * wait for chakra update */}
                        <Link as={ReachLink} to="/user">
                            Users
                        </Link>
                    </MenuItems>
                    <MenuItems>
                        <Link href="https://foundry499.com" isExternal>
                            Blog
                        </Link>
                    </MenuItems>
                </Box>

                <Box display={{ sm: show ? 'block' : 'none', md: 'block' }} mt={{ base: 4, md: 0 }}>
                    <Menu>
                        {/* see: https://github.com/chakra-ui/chakra-ui/issues/205 for prop issue on this button */}
                        <MenuButton bg="transparent" border="1px" as={Button} {...{ rightIcon: 'chevron-down' }}>
                            Account
                        </MenuButton>

                        <MenuList color="black">
                            <AuthContext.Consumer>
                                {({ state }) => {
                                    if (state.user)
                                        return (
                                            <React.Fragment>
                                                <Box px="3">
                                                    <b>{state.user.name}</b>
                                                </Box>
                                                <Box px="3">{state.user.email}</Box>
                                            </React.Fragment>
                                        );
                                }}
                            </AuthContext.Consumer>

                            <MenuDivider />
                            {/*
                        // @ts-ignore * wait for chakra update */}
                            <MenuItem as={ReachLink} to="/settings">
                                Settings
                            </MenuItem>

                            <MenuItem onClick={onLogout}>Logout</MenuItem>
                        </MenuList>
                    </Menu>
                </Box>
            </Flex>
            {role === 'guest' && (
                <Flex
                    align="center"
                    justify="center"
                    wrap="wrap"
                    padding="0.5rem"
                    bg="brand.800"
                    color="white"
                    {...props}
                >
                    <Text>
                        <b>It looks like you're using a guest account!</b> Guests can look around, but you won't be able
                        to submit anything. Feel free to contact us at tharnadek@hey.com
                    </Text>
                </Flex>
            )}
        </>
    );
};

export default Header;
