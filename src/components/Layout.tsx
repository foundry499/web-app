import React, { FunctionComponent } from 'react';
import Header from './Nav';
import { Flex, Box } from '@chakra-ui/core';

const Layout: FunctionComponent<any> = ({ children }) => (
    <main>
        <Header />
        <Flex flexDirection="column" align="center">
            <Box px={3} pt={3} justifySelf="center" width="100vw" maxWidth="1200px">
                {children}
            </Box>
        </Flex>

        <footer></footer>
    </main>
);

export default Layout;
