const API = process.env.REACT_APP_API_LOCATION + '/v1';

export type User = {
    id: number;
    name: string;
    email: string;
    role: 'guest' | 'base' | 'admin';
    createdBy: number;
    createdAt: Date;
    updatedBy: number;
    updatedAt: Date;
};

export type NewUser = {
    name: string;
    email: string;
    password: string;
    role: 'guest' | 'base' | 'admin';
};

// Get a single user
const GetUser = async (id: number): Promise<User> => {
    const response = await fetch(`${API}/user/${id}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Gets all users (json array)
const GetAllUsers = async (): Promise<[User]> => {
    const response = await fetch(`${API}/users`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Gets session cookie
// ({name, email, password, role})
const CreateUser = async (data: NewUser): Promise<User> => {
    const response = await fetch(`${API}/user`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Deletes a user from the DB
// (id)
const DeleteUser = async (id: number): Promise<void> => {
    const response = await fetch(`${API}/user/${id}`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
};

export { GetUser, GetAllUsers, CreateUser, DeleteUser };
