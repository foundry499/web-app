const API = process.env.REACT_APP_API_LOCATION + '/v1';

enum VMType {
    Standard = 0,
    Spot,
    LowPriority,
}

enum VMStatusCode {
    Provisioning = 0,
    Ready,
    Deprovisioning,
    Removed,
}

export type VM = {
    id: number;
    name: string;
    cores: number;
    memory: number;
    storage: number;
    estimatedCost: number;
    vmType: VMType; // 0 = regular, 1 = spot, 2 = low priority
    status: number;
    ip: string;
    createdAt: Date;
    updatedAt: Date;
};

export type VMStatus = {
    id: number;
    vmID: number;
    status: VMStatusCode;
    createdAt: Date;
};

// Gets all vms (json array)
const GetAllVMs = async (): Promise<[VM]> => {
    const response = await fetch(`${API}/vms`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Gets all vm statuses (json array)
const GetAllVMStatuses = async (): Promise<[VMStatus]> => {
    const response = await fetch(`${API}/vms/status`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

export { GetAllVMs, GetAllVMStatuses, VMType, VMStatusCode };
