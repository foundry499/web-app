import { User } from './user';

const API = process.env.REACT_APP_API_LOCATION;

// Gets session cookie
const Login = async (data: { email: string; password: string }): Promise<User> => {
    const response = await fetch(`${API}/login`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Requests session cookie deactivated
const Logout = async (): Promise<void> => {
    const response = await fetch(`${API}/logout`, {
        method: 'POST',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
};

// Gets a fresh cookie (hot out of the oven)
const RefreshSession = async (): Promise<User> => {
    const response = await fetch(`${API}/refresh`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

export { Login, Logout, RefreshSession };
