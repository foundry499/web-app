import { VMType } from './vm';

const API = process.env.REACT_APP_API_LOCATION + '/v1';

enum JobStatusCode {
    Submitted = 0,
    Assigned,
    Pending,
    Running,
    Success,
    Failure,
}

export type Job = {
    id: number;
    name: string;
    tags: object;
    dockerImage: string;
    startCommand: string;
    envVars: any; // this should always be an object
    VMType: VMType;
    storage: number;
    cores: number;
    memory: number;
    containerId: string;
    logsStdout: string;
    logsStderr: string;
    exitCode: number;
    status: number;
    createdBy: number;
    createdAt: Date;
    updatedBy: number;
    updatedAt: Date;
    isDeleted: boolean;
};

export type NewJob = {
    name: string;
    tags: object;
    dockerImage: string;
    startCommand: string;
    envVars: object;
    VMType: VMType; // 0 = regular, 1 = spot, 2 = low priority
    storage: number;
    cores: number;
    memory: number;
};

export type JobStatus = {
    id: number;
    jobID: number;
    status: number;
    createdAt: Date;
};

// Get a single job
const GetJob = async (id: number): Promise<Job> => {
    const response = await fetch(`${API}/job/${id}`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Gets all jobs (json array)
const GetAllJobs = async (): Promise<[Job]> => {
    const response = await fetch(`${API}/jobs`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Creates a new job and returns the associated job object
const CreateJob = async (data: NewJob): Promise<Job> => {
    console.log(JSON.stringify(data));
    const response = await fetch(`${API}/job`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Deletes a job from the DB
const DeleteJob = async (id: number): Promise<void> => {
    const response = await fetch(`${API}/job/${id}`, {
        method: 'DELETE',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
};

const GetJobStatus = async (id: number): Promise<[JobStatus]> => {
    const response = await fetch(`${API}/job/${id}/status`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

// Creates a new job and returns the associated job object
const CreateJobStatus = async (id: number, data: JobStatus): Promise<void> => {
    const response = await fetch(`${API}/job/${id}/status`, {
        method: 'POST',
        mode: 'cors',
        credentials: 'include',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
    });
    if (!response.ok) throw Error(response.statusText);
};

// Gets all jobs (json array)
const GetAllJobStatuses = async (): Promise<[JobStatus]> => {
    const response = await fetch(`${API}/jobs/status`, {
        method: 'GET',
        mode: 'cors',
        credentials: 'include',
    });
    if (!response.ok) throw Error(response.statusText);
    return response.json();
};

export { GetJob, GetAllJobs, CreateJob, DeleteJob, GetJobStatus, CreateJobStatus, JobStatusCode, GetAllJobStatuses };
