import React, { useReducer, useEffect, useState, createContext } from 'react';
import { Router, RouteComponentProps } from '@reach/router';
import { Grid, Spinner, Heading, Stack } from '@chakra-ui/core';

import { RefreshSession } from './api/session';
import Layout from './components/Layout';
import Home from './pages/Home';
import Settings from './pages/Settings';
import JobContainer from './pages/job/JobContainer';
import UserContainer from './pages/user/UserContainer';
import { LoginPage } from './pages/Login';
import { User, GetAllUsers } from 'api/user';
import { GetAllJobs, GetAllJobStatuses } from 'api/job';
import { Reducer, InitialState } from 'store';

type AuthStateType = {
    isAuth: boolean;
    user?: User | null;
};

type AuthAction = { type: 'LOGIN'; payload: User } | { type: 'LOGOUT' };

const AuthInitialState = {
    isAuth: false,
    user: null,
};

export const AuthContext = createContext<{
    state: AuthStateType;
    dispatch: React.Dispatch<any>;
}>({
    state: AuthInitialState,
    dispatch: () => null,
}); // auth context contains

const AuthReducer = (state: AuthStateType, action: AuthAction) => {
    switch (action.type) {
        case 'LOGIN':
            localStorage.setItem('user', JSON.stringify(action.payload));
            return {
                ...state,
                isAuth: true,
                user: action.payload,
            };
        case 'LOGOUT':
            localStorage.clear();
            return {
                ...state,
                isAuth: false,
                user: null,
            };
        default:
            return state;
    }
};

const App = () => {
    const [state, dispatchAuth] = useReducer(AuthReducer, AuthInitialState);
    const [data, dispatch] = useReducer(Reducer, InitialState);
    const [isLoading, setLoading] = useState(true);

    const [user] = React.useState(localStorage.getItem('user') || '');
    useEffect(() => {
        if (user !== '') {
            dispatchAuth({
                type: 'LOGIN',
                payload: JSON.parse(user),
            });
            refresh();
        }
    }, []);

    const refresh = async () => {
        try {
            const user = await RefreshSession();
            dispatchAuth({
                type: 'LOGIN',
                payload: user,
            });
        } catch (err) {
            console.log(err);
            dispatchAuth({
                type: 'LOGOUT',
            });
        }
    };
    const fetchJobs = async () => {
        try {
            const res = await GetAllJobs();
            if (res !== null) {
                res.forEach((job) =>
                    dispatch({
                        type: 'UPDATE_JOB',
                        payload: job,
                    }),
                );
            }
        } catch (err) {
            if (err) console.log(err);
        }
    };
    const fetchJobStatus = async () => {
        try {
            const res = await GetAllJobStatuses();
            if (res !== null) {
                res.forEach((status) =>
                    dispatch({
                        type: 'UPDATE_JOB_STATUS',
                        payload: status,
                    }),
                );
            }
        } catch (err) {
            if (err) console.log(err);
        }
    };
    const fetchUsers = async () => {
        try {
            const res = await GetAllUsers();
            res.forEach((user) =>
                dispatch({
                    type: 'UPDATE_USER',
                    payload: user,
                }),
            );
        } catch (err) {
            if (err) console.log(err);
        }
    };
    // get all users from the API on initial load
    useEffect(() => {
        setLoading(true);
        if (state.isAuth) {
            Promise.all([fetchJobs(), fetchJobStatus(), fetchUsers()]).then(() => {
                setLoading(false);
            });
        }
        //eslint
    }, [state.isAuth]);

    // useInterval(() => {
    //     if (state.isAuth) {
    //         fetchJobs();
    //         fetchUsers();
    //     }
    // }, 15000); // run on a 15 second interval

    let HomeRoute = (props: RouteComponentProps) => <Home />;
    let SettingsRoute = (props: RouteComponentProps) => <Settings />;
    let UserRoute = (props: RouteComponentProps) => <UserContainer state={data} dispatch={dispatch} />;
    let JobRoute = (props: RouteComponentProps) => <JobContainer state={data} dispatch={dispatch} />;

    if (!state.isAuth) {
        return (
            <AuthContext.Provider
                value={{
                    state,
                    dispatch: dispatchAuth,
                }}
            >
                <LoginPage />
            </AuthContext.Provider>
        );
    }
    if (isLoading) {
        return (
            <Grid w="100vw" h="100vh" alignContent="center" justifyContent="center" bg="brand.900">
                <Stack alignItems="center">
                    <Spinner thickness="4px" speed="0.65s" emptyColor="gray.200" color="gray.400" size="xl" />
                    <Heading color="white" size="sm">
                        Loading Foundry499 Data
                    </Heading>
                </Stack>
            </Grid>
        );
    }
    return (
        <AuthContext.Provider
            value={{
                state,
                dispatch: dispatchAuth,
            }}
        >
            <Layout>
                <Router>
                    <HomeRoute path="/" />
                    <SettingsRoute path="/settings" />
                    <JobRoute path="/job/*" />
                    <UserRoute path="/user/*" />
                </Router>
            </Layout>
        </AuthContext.Provider>
    );
};

export default App;
