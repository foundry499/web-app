import React from 'react';
import {
    FormErrorMessage,
    FormLabel,
    FormControl,
    Input,
    Heading,
    Box,
    Flex,
    Select,
    Textarea,
    Button,
    Divider,
} from '@chakra-ui/core';
import yaml from 'js-yaml';
import { useForm } from 'react-hook-form';
import { NewJob } from 'api/job';

// Constants for form data
var AZURE_MIN_CORES = 1;
var AZURE_MAX_CORES = 32;
var AZURE_MIN_STORAGE = 8 * 1024; // 8 GB
var AZURE_MAX_STORAGE = 10000 * 1024; // 10TB
var AZURE_MIN_MEMORY = 1024; // 1GB
var AZURE_MAX_MEMORY = 32 * 1024; // 32 GB

export type NewJobFormData = {
    name: string;
    tags: string[];
    dockerImage: string;
    startCommand: string;
    envVars: any;
    VMType: number; // 0 = regular, 1 = spot, 2 = low priority
    storage: any;
    cores: any;
    memory: any;
};

type JobNewProps = {
    handleAddJob(data: NewJob): void;
};

const JobNew = ({ handleAddJob }: JobNewProps) => {
    const { handleSubmit, errors, register, formState } = useForm<NewJobFormData>();

    function validateName(name: string) {
        let error;
        if (!name) {
            error = 'required';
        }
        return error || true;
    }

    // taken stackoverflow: https://stackoverflow.com/a/3710506/13816637
    function validateJSON(str: string) {
        try {
            JSON.parse(str);
            return true;
        } catch (e) {
            let x = yaml.safeLoad(str);
            if (x === undefined) return 'must be valid json or yaml';
            return true;
        }
    }

    function validateMinMax(str: string, min: number, max: number) {
        let num = parseInt(str);
        if (Number.isNaN(num)) {
            return 'must be number';
        }
        if (num > max) {
            return `max size ${max}`;
        }
        if (num < min) {
            return `min size ${min}`;
        }
        return true;
    }
    const validateMemory = (str: string) => validateMinMax(str, AZURE_MIN_MEMORY, AZURE_MAX_MEMORY);
    const validateCores = (str: string) => validateMinMax(str, AZURE_MIN_CORES, AZURE_MAX_CORES);
    const validateStorage = (str: string) => validateMinMax(str, AZURE_MIN_STORAGE, AZURE_MAX_STORAGE);

    // TODO: ADD TAGS

    const onSubmit = handleSubmit((data: NewJobFormData) => {
        const formattedData = { ...data, tags: {} };
        let err, x;
        try {
            x = JSON.parse(data.envVars);
        } catch (e) {
            x = yaml.safeLoad(data.envVars);
        }
        formattedData.envVars = x;
        formattedData.cores = parseInt(formattedData.cores);
        formattedData.memory = parseInt(formattedData.memory);
        formattedData.storage = parseInt(formattedData.storage);

        if (!err) handleAddJob(formattedData);
    });

    return (
        <Box>
            <Flex justifyContent="space-between">
                <Heading>New Job</Heading>
            </Flex>
            <Divider />

            <form onSubmit={onSubmit} autoComplete="off">
                <FormControl isInvalid={errors.name !== undefined}>
                    <FormLabel htmlFor="name">Name</FormLabel>
                    <Input
                        color="gray.900"
                        name="name"
                        placeholder="Job Name"
                        ref={register({ validate: validateName })}
                    />
                    <FormErrorMessage>{errors.name && errors.name.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.dockerImage !== undefined}>
                    <FormLabel htmlFor="dockerImage">Docker Image URL</FormLabel>
                    <Input
                        color="gray.900"
                        name="dockerImage"
                        placeholder="http://"
                        ref={register({ required: 'required' })}
                    />
                    <FormErrorMessage>{errors.dockerImage && errors.dockerImage.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.startCommand !== undefined}>
                    <FormLabel htmlFor="startCommand">Container Entry Point</FormLabel>
                    <Input
                        color="gray.900"
                        name="startCommand"
                        placeholder="./start"
                        ref={register({ required: false })}
                    />
                    <FormErrorMessage>{errors.startCommand && errors.startCommand.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.envVars !== undefined}>
                    <FormLabel htmlFor="envVars">Environment Variables</FormLabel>
                    <Textarea
                        color="gray.900"
                        name="envVars"
                        placeholder={'ENV_VAR_1: value1\nENV_VAR_2: value2\n'}
                        ref={register({ validate: validateJSON })}
                    />
                    <FormErrorMessage>{errors.envVars && errors.envVars.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.VMType !== undefined}>
                    <FormLabel htmlFor="vmType">VM Type</FormLabel>
                    <Select defaultValue={0} color="gray.900" name="VMType" ref={register()}>
                        <option value={0}>Standard</option>
                        <option value={1}>Spot</option>
                    </Select>
                    <FormErrorMessage>{errors.VMType && errors.VMType.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.storage !== undefined}>
                    <FormLabel htmlFor="storage">Required Storage (MiB)</FormLabel>
                    <Input
                        color="gray.900"
                        name="storage"
                        placeholder="51200"
                        ref={register({ required: 'required', validate: validateStorage })} // max 2TB?
                    />
                    <FormErrorMessage>{errors.storage && errors.storage.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.memory !== undefined}>
                    <FormLabel htmlFor="memory">Required Memory (MiB)</FormLabel>
                    <Input
                        color="gray.900"
                        name="memory"
                        placeholder="16384"
                        ref={register({ required: 'required', validate: validateMemory })} // max 2TB?
                    />
                    <FormErrorMessage>{errors.memory && errors.memory.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.cores !== undefined}>
                    <FormLabel htmlFor="cores">Required Cores</FormLabel>
                    <Input
                        color="gray.900"
                        name="cores"
                        placeholder="4"
                        ref={register({ required: 'required', validate: validateCores })} // max 2TB?
                    />
                    <FormErrorMessage>{errors.cores && errors.cores.message}</FormErrorMessage>
                </FormControl>
                {/* 
                <FormControl isInvalid={errors.tag !== undefined}>
                    <FormLabel htmlFor="tag">Tag</FormLabel>
                    <Input
                        color="gray.900"
                        name="tag"
                        placeholder="Big Run"
                        ref={register({ validate: validateTag })}
                    />
                    <FormErrorMessage>{errors.tag && errors.tag.message}</FormErrorMessage>
                </FormControl> */}

                <Button mt={4} w="100%" variantColor="green" isLoading={formState.isSubmitting} type="submit">
                    Create
                </Button>
            </form>
        </Box>
    );
};

export default JobNew;
