import React from 'react';
import { Stack, Text, Heading } from '@chakra-ui/core';

type JobEditProps = {
    id: string | undefined; // string is parsed from the url
    dispatch: React.Dispatch<any>; // TODO: lift logic up into container
};

const JobEdit = ({ id, dispatch }: JobEditProps) => {
    return (
        <Stack>
            <Heading>Edit Job {id}</Heading>
            <Text fontSize="xl" fontWeight="bold">
                <span role="img" aria-label="construction barrier">
                    🚧
                </span>{' '}
                Under Construction!{' '}
                <span role="img" aria-label="construction worker">
                    👷‍♀️
                </span>
            </Text>
        </Stack>
    );
};

export default JobEdit;
