import React, { Dispatch } from 'react';
import { Router, navigate, RouteComponentProps } from '@reach/router';
import { useToast } from '@chakra-ui/core';

import { NewJob, CreateJob, DeleteJob } from 'api/job';
import { State, Action } from 'store';

import JobEdit from './JobEdit';
import JobDetail from './JobDetail';
import JobNew from './JobNew';
import JobDashboard from './JobDashboard';

type JobContainerProps = {
    state: State;
    dispatch: Dispatch<Action>;
};

// react component
const JobContainer = ({ state, dispatch }: JobContainerProps) => {
    const toast = useToast();

    const handleAddJob = async (data: NewJob) => {
        try {
            const job = await CreateJob(data);
            dispatch({
                type: 'ADD_JOB',
                payload: job,
            });
            navigate('/job');
            toast({
                title: 'Job created.',
                description: "We've created a job with the name " + data.name,
                status: 'success',
                duration: 9000,
                isClosable: true,
            });
        } catch (Error) {
            toast({
                title: 'Job not created.',
                description: 'Could not create a job with the name ' + data.name,
                status: 'error',
                duration: 9000,
                isClosable: true,
            });
        }
    };

    const handleDelJob = async (id: number) => {
        const jobName = state.jobs.find((job) => job.id === id)!.name;
        try {
            await DeleteJob(id);
            dispatch({
                type: 'DEL_JOB',
                payload: id,
            });
            navigate('/job');
            toast({
                title: 'Job deleted.',
                description: "We've deleted the job named " + jobName,
                status: 'success',
                duration: 9000,
                isClosable: true,
            });
        } catch (Error) {
            toast({
                title: 'Job not deleted.',
                description: 'Could not delete the job named ' + jobName,
                status: 'error',
                duration: 9000,
                isClosable: true,
            });
        }
    };

    // required for ReachRouter compat with typescript
    interface RouteProps extends RouteComponentProps {
        id?: string;
    }

    let Dashboard = (props: RouteComponentProps) => <JobDashboard handleDelJob={handleDelJob} jobs={state.jobs} />;
    let Detail = (props: RouteProps) => <JobDetail arg={props.id} jobs={state.jobs} jobStatus={state.jobStatus} />;
    let New = (props: RouteComponentProps) => <JobNew handleAddJob={handleAddJob} />;
    let Edit = (props: RouteProps) => <JobEdit dispatch={dispatch} id={props.id} />;

    return (
        <Router>
            <Dashboard path="/" />
            <Detail path="/:id" />
            <New path="/new" />
            <Edit path="/:id/edit" />
        </Router>
    );
};

export default JobContainer;
