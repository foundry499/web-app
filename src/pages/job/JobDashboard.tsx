import React, { useState } from 'react';
import moment from 'moment';
import { Input, Heading, Text, Box, Flex, IconButton, Button, Grid, Divider } from '@chakra-ui/core';
import { Link } from '@reach/router';
import DeleteButton from 'components/DeleteButton';

import { Job, JobStatusCode } from 'api/job';

type JobDashboardProps = {
    jobs: Job[];
    handleDelJob(id: number): void;
};

const JobDashboard = ({ jobs, handleDelJob }: JobDashboardProps) => {
    const [filter, setFilter] = useState('');

    const filteredJobs = (): Job[] => {
        let tokens = filter.split(':');

        if (tokens.length > 1) {
            let field = tokens[0];
            let string = tokens[1];
            switch (field) {
                case 'name':
                    return jobs.filter((job) => job.name.toLowerCase().includes(string.toLowerCase()));
                case 'status':
                    return jobs.filter((job) => job.status.toString().toLowerCase().includes(string.toLowerCase()));
                default:
                    break;
            }
        }
        return jobs.filter(
            (job) =>
                job.name.toLowerCase().includes(filter.toLowerCase()) ||
                job.status.toString().toLowerCase().includes(filter.toLowerCase()),
        );
    };

    const coloredStatus = (code: JobStatusCode) => {
        switch (code) {
            case JobStatusCode.Assigned:
                return <Text color="yellow.500">Assigned</Text>;
            case JobStatusCode.Pending:
                return <Text color="yellow.500">Pending</Text>;
            case JobStatusCode.Running:
                return <Text color="black.500">Running</Text>;
            case JobStatusCode.Success:
                return <Text color="green.500">Success</Text>;
            case JobStatusCode.Failure:
                return <Text color="red.500">Failed</Text>;
            default:
                return <Text color="gray.500">Submitted</Text>;
        }
    };

    return (
        <Box>
            <Flex justifyContent="space-between">
                <Heading>Jobs</Heading>
                <Link to="./new">
                    <Button variantColor="green" aria-label="add new job">
                        Add Job
                    </Button>
                </Link>
            </Flex>
            <Divider />
            <Flex justifyContent="space-between">
                <Input
                    placeholder="Filter . . ."
                    type="text"
                    value={filter}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setFilter(e.currentTarget.value)}
                />
            </Flex>
            {jobs.length === 0 && (
                <Flex w="100%" h="360px" alignItems="center" justifyContent="center">
                    <Heading size="md">
                        <span role="img" aria-label="sad emoji">
                            😥
                        </span>{' '}
                        Nothing to show here...
                    </Heading>
                </Flex>
            )}
            {filteredJobs()
                .sort((a, b) => (a.createdAt < b.createdAt ? 1 : -1))
                .map((job) => (
                    <Grid gridTemplateColumns="1fr 1fr 1fr 100px" gap={[6, 1]} key={job.id}>
                        <Divider gridColumn="span 4" />
                        <Box w="100%">
                            <Heading as="h2" size="md">
                                <Link to={'./' + job.id}>{job.name}</Link>
                            </Heading>
                        </Box>
                        <Box w="100%">
                            <Text fontSize="sm">
                                <b>Status</b>
                            </Text>
                            <Box fontSize="lg">{coloredStatus(job.status)}</Box>
                        </Box>

                        <Box w="100%">
                            <Text fontSize="sm">
                                <b>Created</b>
                            </Text>
                            {moment(job.createdAt).fromNow()}
                        </Box>
                        <Flex alignItems="center" justifyContent="space-between">
                            <Link to={'./' + job.id + '/edit'}>
                                <IconButton
                                    variant="outline"
                                    variantColor="gray"
                                    aria-label="modify job"
                                    fontSize="20px"
                                    icon="edit"
                                ></IconButton>
                            </Link>

                            <DeleteButton
                                msg="Are you sure you want to delete this job? This cannot be reversed."
                                handleDelete={() => handleDelJob(job.id)}
                            />
                        </Flex>
                    </Grid>
                ))}
        </Box>
    );
};

export default JobDashboard;
