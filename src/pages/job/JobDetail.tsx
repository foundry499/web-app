import React, { useEffect } from 'react';
import { Heading, Text, Box, Flex, Divider, Stack, Grid, Code, Tag, Collapse, Button } from '@chakra-ui/core';
import moment from 'moment';
import { Job, JobStatus, JobStatusCode } from 'api/job';

// create state
type JobHistoryType = Job[];

type JobDetailProps = {
    arg: string | undefined; // string is parsed from the url
    jobs: Job[];
    jobStatus: JobStatus[];
};

const JobDetails = ({ arg, jobs, jobStatus }: JobDetailProps) => {
    const id = arg !== undefined ? parseInt(arg) : 0; // job with id 0 will never exist
    const job = jobs.find((job) => job.id === id);
    const history = jobStatus.filter((status) => status.jobID === id).sort((a, b) => {let ta = moment(a.createdAt); let tb = moment(b.createdAt); return tb.diff(ta)});

    if (id === 0 || job === undefined) return <Heading>Job Not Found</Heading>;

    return (
        <Stack>
            <Flex justify="space-between">
                <Box>
                    <Heading size="lg">{job.name}</Heading>
                    <Heading size="xs">Job ID: {job.id}</Heading>
                </Box>
                <Box>
                    Status: {coloredStatus(job.status)}
                </Box>
            </Flex>
            <Divider />
            <Heading size="md">Docker Container</Heading>
            <Text>
                Docker Image: <Code>{job.dockerImage}</Code>
            </Text>
            <Text>
                Environment Variables: <Code>{JSON.stringify(job.envVars)}</Code>
            </Text>

            <Text>
                Start Command: <Code>{job.startCommand}</Code>
            </Text>

            <Text>
                Required Cores: <Code>{job.cores}</Code>
            </Text>
            <Text>
                Required Memory: <Code>{job.memory} MiB</Code>
            </Text>
            <Text>
                Required Storage: <Code>{job.storage} MiB</Code>
            </Text>
            

            <Divider/>
            <Heading size="md">Job History</Heading>
            <Grid templateColumns="repeat(2, 1fr)" gap={6}>
                {history.map((status) => <JobStatusItem jobStatus={status}/>)}
                
            </Grid>

            <Divider/>
            <Heading size="md">Logs</Heading>
            <LogBox logs={job.logsStderr} name="Stderr Logs"/>
            <LogBox logs={job.logsStdout} name="Stdout Logs"/>
        </Stack>
    );
};

type JobStatusProps = {
    jobStatus: JobStatus;
};

const JobStatusItem = ({ jobStatus }: JobStatusProps) => (
    <>
        <Box>{coloredStatus(jobStatus.status)}</Box>
        <Box>{moment(jobStatus.createdAt).fromNow()}</Box>
    </>
)

type LogBoxProps = {
    logs: any;
    name: string;
}

const LogBox = ({logs, name}: LogBoxProps) => {
    const [show, setShow] = React.useState(false);
    const handleToggle = () => setShow(!show);
    return (
    <>
        <Collapse startingHeight={20} isOpen={show}>
            <Code>  
                {JSON.stringify(logs)}
            </Code>
        </Collapse>
        <Button size="sm" onClick={handleToggle} mt="1rem">
        {show ? `Hide ${name}` : `Show ${name}`}
        </Button>
    </>
    );
}

const coloredStatus = (code: JobStatusCode) => {
    switch (code) {
        case JobStatusCode.Assigned:
            return <Tag variantColor="yellow">Assigned</Tag>;
        case JobStatusCode.Pending:
            return <Tag variantColor="yellow">Pending</Tag>;
        case JobStatusCode.Running:
            return <Tag variantColor="blue">Running</Tag>;
        case JobStatusCode.Success:
            return <Tag variantColor="green">Success</Tag>;
        case JobStatusCode.Failure:
            return <Tag variantColor="red">Failed</Tag>;
        default:
            return <Tag variantColor="gray">Submitted</Tag>;
    }
};

export default JobDetails;
