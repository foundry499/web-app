import React from 'react';
import { Text } from '@chakra-ui/core';

type UserEditProps = {
    id: string | undefined; // string is parsed from the url
    dispatch: React.Dispatch<any>; // TODO: lift logic up into container
};

const UserEdit = ({ id, dispatch }: UserEditProps) => {
    return <Text>UserID: {id}</Text>;
};

export default UserEdit;
