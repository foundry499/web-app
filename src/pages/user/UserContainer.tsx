import React, { Dispatch } from 'react';
import { Router, navigate, RouteComponentProps } from '@reach/router';
import { useToast } from '@chakra-ui/core';

import { NewUser, CreateUser, DeleteUser } from 'api/user';
import { State, Action } from 'store';

import UserEdit from './UserEdit';
import UserNew from './UserNew';
import UserDashboard from './UserDashboard';

type UserContainerProps = {
    state: State;
    dispatch: Dispatch<Action>;
};

// react component
const UserContainer = ({ state, dispatch }: UserContainerProps) => {
    const toast = useToast();

    const handleAddUser = async (data: NewUser) => {
        try {
            const user = await CreateUser(data);
            dispatch({
                type: 'ADD_USER',
                payload: user,
            });
            navigate('/user');
            toast({
                title: 'Account created.',
                description: "We've created an account for " + data.name,
                status: 'success',
                duration: 9000,
                isClosable: true,
            });
        } catch (Error) {
            toast({
                title: 'Account not created.',
                description: 'Could not create an account for ' + data.name,
                status: 'error',
                duration: 9000,
                isClosable: true,
            });
        }
    };

    const handleDelUser = async (id: number) => {
        const userName = state.users.find((user) => user.id === id)!.name;
        try {
            await DeleteUser(id);
            dispatch({
                type: 'DEL_USER',
                payload: id,
            });
            navigate('/user');
            toast({
                title: 'Account deleted.',
                description: "We've deleted the account for " + userName,
                status: 'success',
                duration: 9000,
                isClosable: true,
            });
        } catch (Error) {
            toast({
                title: 'Account not deleted.',
                description: 'Could not delete ' + userName + "'s account.",
                status: 'error',
                duration: 9000,
                isClosable: true,
            });
        }
    };

    // required for ReachRouter compat with typescript
    interface EditProps extends RouteComponentProps {
        id?: string;
    }

    let Dashboard = (props: RouteComponentProps) => <UserDashboard handleDelUser={handleDelUser} users={state.users} />;
    let New = (props: RouteComponentProps) => <UserNew handleAddUser={handleAddUser} />;
    let Edit = (props: EditProps) => <UserEdit dispatch={dispatch} id={props.id} />;

    return (
        <Router>
            <Dashboard path="/" />
            <New path="/new" />
            <Edit path="/:id/edit" />
        </Router>
    );
};

export default UserContainer;
