import React, { useState } from 'react';
import moment from 'moment';
import { Input, Heading, Text, Box, Flex, IconButton, Button, Grid, Divider } from '@chakra-ui/core';
import { Link } from '@reach/router';
import DeleteButton from 'components/DeleteButton';

import { User } from 'api/user';

type UserDashboardProps = {
    users: User[];
    handleDelUser(id: number): void;
};

const UserDashboard = ({ users, handleDelUser }: UserDashboardProps) => {
    const [filter, setFilter] = useState('');

    return (
        <Box>
            <Flex justifyContent="space-between">
                <Heading>Users</Heading>
                <Link to="./new">
                    <Button variantColor="green" aria-label="add new user">
                        Add User
                    </Button>
                </Link>
            </Flex>
            <Divider />
            <Flex justifyContent="space-between">
                <Input
                    placeholder="Filter . . ."
                    type="text"
                    value={filter}
                    onChange={(e: React.ChangeEvent<HTMLInputElement>) => setFilter(e.currentTarget.value)}
                />
            </Flex>

            <Grid gridTemplateColumns="1fr 1fr 1fr 100px" gap={[6, 1]}>
                {users
                    .filter(
                        (user) =>
                            user.name.toLowerCase().includes(filter.toLowerCase()) ||
                            user.email.toLowerCase().includes(filter.toLowerCase()) ||
                            user.role.toLowerCase().includes(filter.toLowerCase()),
                    )
                    .map((user) => (
                        <React.Fragment key={user.id}>
                            <Divider gridColumn="span 4" />
                            <Box w="100%">
                                <Heading as="h2" size="sm">
                                    {user.name}
                                </Heading>
                                <Text>{user.email}</Text>
                            </Box>
                            <Box w="100%">
                                <Text>
                                    <b>Role</b>
                                </Text>
                                <Text>{user.role}</Text>
                            </Box>

                            <Box w="100%">
                                <Text>
                                    <b>Created</b>
                                </Text>
                                {moment(user.createdAt).fromNow()}
                            </Box>
                            <Flex alignItems="center" justifyContent="space-between">
                                <Link to={'./' + user.id + '/edit'}>
                                    <IconButton
                                        variant="outline"
                                        variantColor="gray"
                                        aria-label="modify job"
                                        fontSize="20px"
                                        icon="edit"
                                    ></IconButton>
                                </Link>
                                {user.id !== 1 && (
                                    <DeleteButton
                                        msg="Are you sure you want to delete this user?"
                                        handleDelete={() => handleDelUser(user.id)}
                                    />
                                )}
                            </Flex>
                        </React.Fragment>
                    ))}
            </Grid>
        </Box>
    );
};

export default UserDashboard;
