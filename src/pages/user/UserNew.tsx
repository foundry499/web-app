import React from 'react';
import {
    FormErrorMessage,
    FormLabel,
    FormControl,
    Input,
    Heading,
    Box,
    Flex,
    Select,
    Button,
    Divider,
} from '@chakra-ui/core';
import validator from 'validator';
import { useForm } from 'react-hook-form';
import { NewUser } from 'api/user';

type UserNewProps = {
    handleAddUser(data: NewUser): void;
};

const UserNew = ({ handleAddUser }: UserNewProps) => {
    const { handleSubmit, errors, register, formState } = useForm<NewUser>();

    function validateName(name: string) {
        let error;
        if (!name) {
            error = 'Must enter name';
        }
        return error || true;
    }

    function validateEmail(email: string) {
        let error;
        if (!email) {
            error = 'Must enter email';
        }
        if (!validator.isEmail(email)) {
            error = 'Invalid email';
        }
        return error || true;
    }
    function validatePassword(password: string) {
        let error;
        if (!password) {
            error = 'Must enter password';
        }
        if (password.length < 6) {
            error = 'Your password must be at least 6 characters';
        }
        return error || true;
    }

    const onSubmit = handleSubmit((data: NewUser) => {
        handleAddUser(data);
    });

    return (
        <Box>
            <Flex justifyContent="space-between">
                <Heading>New User</Heading>
            </Flex>
            <Divider />

            <form onSubmit={onSubmit} autoComplete="off">
                <FormControl isInvalid={errors.name !== undefined}>
                    <FormLabel htmlFor="name">Name</FormLabel>
                    <Input
                        color="gray.900"
                        name="name"
                        placeholder="Your Name"
                        ref={register({ validate: validateName })}
                    />
                    <FormErrorMessage>{errors.name && errors.name.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.email !== undefined}>
                    <FormLabel htmlFor="email">Email</FormLabel>
                    <Input
                        color="gray.900"
                        name="email"
                        placeholder="me@example.com"
                        autoComplete="new-off"
                        ref={register({ validate: validateEmail })}
                    />
                    <FormErrorMessage>{errors.email && errors.email.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.password !== undefined}>
                    <FormLabel htmlFor="name">Password</FormLabel>
                    <Input
                        color="gray.900"
                        name="password"
                        placeholder="Password"
                        type="password"
                        autoComplete="new-password"
                        ref={register({ validate: validatePassword })}
                    />
                    <FormErrorMessage>{errors.password && errors.password.message}</FormErrorMessage>
                </FormControl>

                <FormControl isInvalid={errors.role !== undefined}>
                    <FormLabel htmlFor="role">Role</FormLabel>
                    <Select defaultValue="base" color="gray.900" name="role" placeholder="Select role" ref={register()}>
                        <option value="base">User</option>
                        <option value="guest">Guest</option>
                        <option value="admin">Admin</option>
                    </Select>
                    <FormErrorMessage>{errors.role && errors.role.message}</FormErrorMessage>
                </FormControl>

                <Button mt={4} w="100%" variantColor="green" isLoading={formState.isSubmitting} type="submit">
                    Create
                </Button>
            </form>
        </Box>
    );
};

export default UserNew;
