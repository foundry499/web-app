import React from 'react';
import { Stack, Flex, Heading, Text, Link, Divider, Box } from '@chakra-ui/core';
import { Link as ReachLink } from '@reach/router';

const Home = () => (
    <Stack>
        {' '}
        <Flex justifyContent="space-between">
            <Heading>Welcome to Foundry 499</Heading>
        </Flex>
        <Divider />
        <Text>
            This project is a demo. Functionality subject to change at any time. Please see{' '}
            <Link href="https://www.foundry499.com">foundry499.com</Link> for more information.
        </Text>
        <Divider />
        <Box>
            <ReachLink to="/job">
                <Heading as="h2" size="md">
                    Go to Jobs
                </Heading>
                <Text>View jobs, add new jobs, or edit an existing job.</Text>
            </ReachLink>
        </Box>
        <Divider />
        <Box>
            <ReachLink to="/user">
                <Heading as="h2" size="md">
                    Go to Users
                </Heading>
                <Text>View and manage users.</Text>
            </ReachLink>
        </Box>
    </Stack>
);

export default Home;
