import React, { useEffect } from 'react';
import { AuthContext } from '../App';
import {
    FormErrorMessage,
    FormLabel,
    FormControl,
    Input,
    Button,
    Flex,
    Box,
    Heading,
    Link,
    ButtonProps,
    useToast,
} from '@chakra-ui/core';

import validator from 'validator';
import { useForm } from 'react-hook-form';
import { Login, Logout } from '../api/session';

type Credentials = {
    email: string;
    password: string;
};

const LoginPage = () => {
    const toast = useToast();
    const { dispatch } = React.useContext(AuthContext);
    const { handleSubmit, errors, register, formState } = useForm<Credentials>({
        mode: 'onSubmit',
        reValidateMode: 'onSubmit',
    });

    const onSubmit = handleSubmit(async (data: Credentials) => {
        try {
            const user = await Login(data);
            dispatch({
                type: 'LOGIN',
                payload: user,
            });
        } catch (err) {
            toast({
                title: 'Could not log in',
                description: `Error logging in: ${err.message}`,
                status: 'error',
                duration: 9000,
                isClosable: true,
            });
        }
    });

    return (
        <Flex backgroundColor="brand.900" color="white" h="100vh" direction="column" align="center" justify="center">
            <Heading p="6">
                <b>Login</b>
            </Heading>
            <Box w="sm" borderWidth="1px" rounded="lg" overflow="hidden" p="6">
                <form onSubmit={onSubmit}>
                    <FormControl isInvalid={errors.email !== undefined}>
                        <FormLabel htmlFor="email">Email</FormLabel>
                        <Input
                            color="gray.900"
                            name="email"
                            placeholder="me@example.com"
                            ref={register({
                                validate: (e) => (validator.isEmail(e) ? true : 'Must enter a valid email'),
                            })}
                        />
                        <FormErrorMessage>{errors.email && errors.email.message}</FormErrorMessage>
                    </FormControl>

                    <FormControl isInvalid={errors.password !== undefined}>
                        <FormLabel htmlFor="name">Password</FormLabel>
                        <Input
                            color="gray.900"
                            name="password"
                            placeholder="Password"
                            type="password"
                            ref={register({
                                validate: (p) => (p !== '' ? true : 'Must enter a password'),
                            })}
                        />
                        <FormErrorMessage>{errors.password && errors.password.message}</FormErrorMessage>
                    </FormControl>

                    <Button mt={4} w="100%" variant="outline" isLoading={formState.isSubmitting} type="submit">
                        Login
                    </Button>
                </form>
            </Box>
            <Link color="gray.200" href="http://www.foundry499.com">
                Foundry499 Homepage
            </Link>
        </Flex>
    );
};

const LogoutButton = (props: ButtonProps) => {
    const { dispatch } = React.useContext(AuthContext);
    function onClick(e: React.MouseEvent) {
        e.preventDefault();
        return Logout().then(() => {
            dispatch({
                type: 'LOGOUT',
            });
        });
    }

    return (
        <Button onClick={onClick} {...props} type="submit">
            {props.children}
        </Button>
    );
};

export { LoginPage, LogoutButton };
