import { Job, JobStatus } from './api/job';
import { User } from './api/user';

export type State = {
    jobs: Job[];
    jobStatus: JobStatus[];
    users: User[];
};

export const InitialState = {
    jobs: [],
    jobStatus: [],
    users: [],
};

export type Action = {
    type: string;
    payload: any;
};

// Reducer is responsible for handling actions and returning
// an updated state object.
export const Reducer = (state: State, action: Action) => {
    switch (action.type) {
        // Jobs Reducer Section
        case 'ADD_JOB':
            // payload: Job
            return { ...state, jobs: [...state.jobs, action.payload] };
        case 'UPDATE_JOB':
            // payload: Job
            return { ...state, jobs: [...state.jobs.filter((job) => job.id !== action.payload.id), action.payload] };
        case 'DEL_JOB':
            // payload: Job id
            return { ...state, jobs: [...state.jobs.filter((job) => job.id !== action.payload)] };
        case 'UPDATE_JOB_STATUS':
            // payload: Job status
            return {
                ...state,
                jobStatus: [...state.jobStatus.filter((status) => status.id !== action.payload), action.payload],
            };
        // Users Reducer Section
        case 'ADD_USER':
            // payload: user
            return { ...state, users: [...state.users, action.payload] };
        case 'UPDATE_USER':
            // payload: User
            return {
                ...state,
                users: [...state.users.filter((user) => user.id !== action.payload.id), action.payload],
            };
        case 'DEL_USER':
            // payload: user id
            return { ...state, users: [...state.users.filter((user) => user.id !== action.payload)] };

        default:
            return state;
    }
};
