import { theme } from '@chakra-ui/core';
// custom typefaces
import 'typeface-work-sans';
import 'typeface-open-sans';
import 'typeface-fira-code';

const customTheme = {
    ...theme,
    fonts: {
        ...theme.fonts,
        body: 'Open Sans, system-ui, sans-serif',
        heading: 'Work Sans, Segoe UI, sans-serif',
        mono: 'Fira Code, monospace',
    },
    colors: {
        ...theme.colors,
        brand: {
            900: '#24305E',
            800: '#374785',
            700: '#A8D0E6',
            accent: '#F76C6C',
            subtle: '#F76C6C',
        },
    },
};

export default customTheme;
