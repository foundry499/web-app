## Prerequisites

You need to have [NodeJS](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/) installed.

## Developer Guide

1. Create a .env file or set an environment variable for the API location: `echo "REACT_APP_API_LOCATION=http://localhost:8080" >> .env` works well for development.

2. Run `yarn` to install dependencies

3. Run `yarn start` to start the build server

## Available Scripts

In the project directory, you can run:

| Command      | Purpose                                                                                                                                 |
| ------------ | --------------------------------------------------------------------------------------------------------------------------------------- |
| `yarn start` | Start the development server with hot-reloading enabled. Open [http://localhost:3000](http://localhost:3000) to view it in the browser. |
| `yarn test`  | Launches the test runner in the interactive watch mode.                                                                                 |
| `yarn build` | Builds the app for production to the `build` folder.                                                                                    |
| `yarn eject` | Eject Create-React-App. See below.                                                                                                      |

## Create-React-App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

### Ejecting: `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.
